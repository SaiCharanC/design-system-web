import { ButtonComponent } from '../../projects/button/src/public-api';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';

// storiesOf('button', module)
//   .add('basic', () => ({
//     component: ButtonComponent,
//     props: {
//       text: 'Basic Button'
//     },
//   }))
//   .add('disabled', () => ({
//     component: ButtonComponent,
//     props: {
//       text: 'Disabled Button',
//       disabled: true
//     },
//     argTypes: {
//       text: { control: 'text' },
//       disabled: { control: 'boolean' },
//     }
//   }));

const defaultButton = {
  text: text('text', 'Button'),
  disabled: boolean('disabled', false)
};

export default {
  title: 'Button',
  decorators: [withKnobs],
};

export const Basic = (args) => ({
  component: ButtonComponent,
  props: {
    text: text('text', 'Basic Button'),
    disabled: boolean('disabled', false)
  },
});

export const Disabled = () => ({
  component: ButtonComponent,
  props: {
    text: text('text', 'Disabled Button'),
    disabled: boolean('disabled', true)
  },
});