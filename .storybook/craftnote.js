import { create } from '@storybook/theming';
import logo from '../src/assets/images/logo.png';

export default create({
    base: 'light',
    brandTitle: 'Craftnote',
    fontBase: '"Open Sans", sans-serif',
    brandImage: logo,
});